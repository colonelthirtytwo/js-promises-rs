
Rust crate for using and manipulating JS Promise objects, using stdweb.

Contains two major types:

* `RawPromise`, a lightweight wrapper for a JS promise. These work with raw `stdweb::Value`s.
  It provides Rust methods for chaining promises with Rust functions, handling cleanup.
* `Promise<TOk, TErr>`, a more featureful wrapper that is capable of sending arbitrary Rust types.
