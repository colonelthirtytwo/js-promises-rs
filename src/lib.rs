//! Bridge between JS promises and Rust's async mechanisms

// Needed for #[async_test]
#![cfg_attr(all(test, target_arch = "wasm32"), feature(linkage))]

use std::{
	borrow::Borrow,
	cell::{
		Cell,
		RefCell,
	},
	cmp::{
		Eq,
		PartialEq,
	},
	collections::HashSet,
	future::Future,
	hash::{
		Hash,
		Hasher,
	},
	mem,
	pin::Pin,
	rc::Rc,
	task::{
		Context,
		Poll,
		RawWaker,
		RawWakerVTable,
		Waker,
	},
};

use stdweb::{
	self,
	console,
	js,
};

/// Global state
struct State {
	// Promises that have awakened
	pending: RefCell<HashSet<Rc<PromiseHandle>>>,
	// Empty hash set, for double-buffering `pending`
	pending_empty: RefCell<HashSet<Rc<PromiseHandle>>>,
	// Do we have a `setTimeout(poll,0)` call registered?
	has_poll_timeout: Cell<bool>,
}
thread_local!(
	/// Global state handle.
	/// 
	/// Ok technically thread local but atm wasm is restricted to one thread.
	/// Will need to be revised when multi-thread WASM comes out.
	static STATE: State = State {
		pending: RefCell::new(HashSet::new()),
		pending_empty: RefCell::new(HashSet::new()),
		has_poll_timeout: Cell::new(false),
	}
);

/// Polls all futures that have been woken up.
fn poll() {
	// Have to be careful with refcell borrows, since future execution can and will
	// wake tasks that have to add to `PENDING`
	STATE.with(|state| {
		// Swap buffers.
		// While we drain the old pending set, give the futures a new empty set to
		// fill.
		RefCell::swap(&state.pending, &state.pending_empty);

		// Poll awakened futures.
		let mut pending = state.pending_empty.borrow_mut();
		for handle in pending.drain() {
			let waker = Rc::clone(&handle).into_waker();
			let mut ctx = Context::from_waker(&waker);
			let mut fut = handle.future.borrow_mut();

			// Don't actually care about this value. Either its `Ready(())` and we just drop it,
			// or its `Pending` and the future registered itself with a waker.
			let _ = fut.as_mut().poll(&mut ctx);
		}
	});
}

/// Callback for `set_timeout` that clears the poll timeout flag and then polls.
fn poll_from_timeout() {
	STATE.with(|state| state.has_poll_timeout.set(false));
	poll();
}

/// Sets up a timeout timer to call poll asap after returning from the JS interpeter.
/// If a timer is already set, does nothing.
fn arm_timeout() {
	STATE.with(|state| {
		if state.has_poll_timeout.get() {
			return;
		}

		stdweb::web::set_timeout(poll_from_timeout, 0);
		state.has_poll_timeout.set(true);
	});
}

/// Executes the passed-in function while preventing the timeout timer to be set, even if
/// `arm_timeout` is called.
///
/// The future `then` callback does this since it polls itself, an extra timeout would be unneccessary.
fn block_arm_timeout<F: FnOnce()>(f: F) {
	STATE.with(|state| {
		let prev = state.has_poll_timeout.replace(true);
		(f)();
		state.has_poll_timeout.set(prev);
	})
}

/// Executes a future, using the JS runtime to execute the future.
pub fn exec<F: Future<Output = ()> + 'static>(fut: F) {
	console!(log, "Exec");
	STATE.with(|state| {
		state
			.pending
			.borrow_mut()
			.insert(Rc::new(PromiseHandle::new(fut)));
		arm_timeout();
	});
	console!(log, "Exec done");
}

/// Internal handle to the promise, always behind an `Rc`.
///
/// Is also the object that the `RawWaker` points to.
struct PromiseHandle {
	/// The future being executed
	future: RefCell<Pin<Box<dyn Future<Output = ()>>>>,
	/// The cached address of the future, for set operations.
	/// The refcell might block access to the future address, so we have to cache it.
	/// Safe to use since the future is pinned.
	addr: *const (),
}
impl PartialEq for PromiseHandle {
	fn eq(&self, other: &Self) -> bool {
		self.addr == other.addr
	}
}
impl Eq for PromiseHandle {}
impl Hash for PromiseHandle {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.addr.hash(state)
	}
}
impl Borrow<*const ()> for PromiseHandle {
	fn borrow(&self) -> &*const () {
		&self.addr
	}
}
impl PromiseHandle {
	/// Creates a raw waker that wakes up this future
	fn into_raw_waker(self: Rc<Self>) -> RawWaker {
		RawWaker::new(Rc::into_raw(self) as *const _, &PROMISE_WAKER_VTABLE)
	}

	/// Creates a waker that wakes up this future
	fn into_waker(self: Rc<Self>) -> Waker {
		unsafe { Waker::from_raw(self.into_raw_waker()) }
	}

	/// Creates a new handle from a future
	fn new<F: Future<Output = ()> + 'static>(fut: F) -> Self {
		let pinned_box = Box::pin(fut);
		let addr = &*pinned_box as *const F as *const ();
		let future = RefCell::new(pinned_box);
		Self { future, addr }
	}

	/// Internal: Wakes up this future
	fn wake(this: &Rc<Self>) {
		console!(log, "Waking");
		STATE.with(|state| {
			state.pending.borrow_mut().insert(Rc::clone(this));
			arm_timeout();
		});
		console!(log, "Waking done");
	}

	/// Internal: casts a pointer to a refcounted promisehandle.
	unsafe fn to_internal(p: *const ()) -> Rc<PromiseHandle> {
		Rc::from_raw(p as *const _)
	}

	/// Internal: RawWakerVTable clone function
	unsafe fn v_clone(p: *const ()) -> RawWaker {
		let rc = Self::to_internal(p);
		let new_ref = Rc::clone(&rc);
		Rc::into_raw(rc); // Keep old reference alive
		new_ref.into_raw_waker()
	}

	/// Internal: RawWakerVTable wake function
	unsafe fn v_wake(p: *const ()) {
		let rc = Self::to_internal(p);
		PromiseHandle::wake(&rc);
	}

	/// Internal: RawWakerVTable wake_by_ref function
	unsafe fn v_wake_by_ref(p: *const ()) {
		let rc = Self::to_internal(p);
		PromiseHandle::wake(&rc);
		Rc::into_raw(rc); // Keep old reference alive
	}

	/// Internal: RawWakerVTable drop function
	unsafe fn v_drop(p: *const ()) {
		mem::drop(Self::to_internal(p));
	}
}
/// RawWaker vtable for PromiseHandle
const PROMISE_WAKER_VTABLE: RawWakerVTable = RawWakerVTable::new(
	PromiseHandle::v_clone,
	PromiseHandle::v_wake,
	PromiseHandle::v_wake_by_ref,
	PromiseHandle::v_drop,
);

/// Wraps a JS promise object, converting it to a Rust future.
///
/// Resolves to `Ok(stdweb::Value)` if the promise resolves, or `Err(stdweb::Value)` if the promise is
/// rejected.
///
/// Promises that never resolve will lead to memory leaks.
pub struct PromiseFuture {
	/// Internal data.
	///
	/// The future object(s)
	internal: Rc<RefCell<PromiseInternals>>,
}
impl PromiseFuture {
	/// Converts a promise to a future.
	///
	/// Panics
	/// ------
	///
	/// May panic if the passed in JS value is not an object or does not have a `then` method.
	pub fn new(promise: stdweb::Value) -> Self {
		let internal = Rc::new(RefCell::new(PromiseInternals {
			fulfilled_value: None,
			waker: None,
		}));

		let cb_internal = Rc::clone(&internal);
		let cb = move |ok: bool, value: stdweb::Value| {
			{
				let mut internal = cb_internal.borrow_mut();
				internal.fulfilled_value = Some(if ok { Ok(value) } else { Err(value) });

				if let Some(waker) = internal.waker.take() {
					block_arm_timeout(move || waker.wake());
				}
			}
			poll();
		};

		js! { @(no_return)
			let cb = @{stdweb::Once(cb)};
			@{promise}.then(function(v) {
				cb(true, v);
			}, function(v) {
				cb(false, v);
			});
		}

		Self { internal }
	}
}

impl Future for PromiseFuture {
	type Output = Result<stdweb::Value, stdweb::Value>;

	fn poll(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
		let mut internal = self.internal.borrow_mut();
		if let Some(v) = internal.fulfilled_value.clone() {
			return Poll::Ready(v);
		}
		internal.waker = Some(cx.waker().clone());
		return Poll::Pending;
	}
}

struct PromiseInternals {
	fulfilled_value: Option<Result<stdweb::Value, stdweb::Value>>,
	waker: Option<Waker>,
}

#[cfg(all(test, target_arch = "wasm32"))]
mod tests {
	use super::*;
	use stdweb::{
		async_test,
		console,
	};

	macro_rules! dfail {
		($done:expr, $($fmtargs:tt)*) => {
			let errmsg = format!($($fmtargs)*);
			console!(error, &errmsg);
			($done)(Err(errmsg));
			panic!("assertion failed");
		}
	}

	/*macro_rules! dassert {
		($done:expr, $test:expr) => {
			if !$test {
				dfail!($done, "{}:{}: assertion failed", line!(), column!());
			}
		};
		($done:expr, $test:expr, $($fmtargs:tt)*) => {
			if !$test {
				dfail!($done, "{}:{}: {}", line!(), column!(), format!($($fmtargs)*));
			}
		};
	}*/

	macro_rules! dassert_eq {
		($done:expr, $a:expr, $b:expr) => {
			let a = $a;
			let b = $b;
			if !(a == b) {
				dfail!($done, "{}:{}: {:?} != {:?}", line!(), column!(), a, b);
				}
		};
	}

	/*use std::panic;
	fn panic_hook(info: &panic::PanicInfo) {
		let mut msg = info.to_string();
		let stack = js!{
			let old_limit = Error.stackTraceLimit;
			Error.stackTraceLimit = 99;
			let err = new Error();
			let stack = err.stack;
			Error.stackTraceLimit = old_limit;
			return stack;
		};
		msg.push_str("\n\nStack:\n\n");
		if let Some(s) = stack.as_str() {
			msg.push_str(s)
		} else {
			msg.push_str(&format!("{:?}", stack));
		}
		msg.push_str("\n\n");

		console!(error, msg);
	}

	pub fn set_panic_hook() {
		panic::set_hook(Box::new(panic_hook));
	}*/

	macro_rules! async_await_test {
		(fn $testname:ident() $body:block) => {
			mod $testname {
				use super::*;
				async fn test_fn() -> Result<(), Box<dyn ::std::error::Error>> $body

				#[async_test]
				fn $testname<F: FnOnce(Result<(), String>)>(done: F) {
					exec(async move {
						let res = test_fn().await;
						done(res.map_err(|e| e.to_string()));
					});
				}

			}
		};
	}

	#[async_test]
	fn resolved_promise<F: FnOnce(Result<(), String>)>(done: F) {
		exec(async move {
			let promise_res = PromiseFuture::new(js! { return Promise.resolve(123); }).await;
			let web_value = match promise_res {
				Ok(v) => v,
				Err(e) => {
					dfail!(done, "Promise rejected with value: {:?}", e);
				}
			};
			dassert_eq!(done, web_value, 123);
			done(Ok(()));
		});
	}

	#[async_test]
	fn rejected_promise<F: FnOnce(Result<(), String>)>(done: F) {
		exec(async move {
			let promise_res = PromiseFuture::new(js! { return Promise.reject(123); }).await;
			let web_value = match promise_res {
				Ok(v) => {
					dfail!(done, "Promise resolved with value: {:?}", v);
				}
				Err(v) => v,
			};
			dassert_eq!(done, web_value, 123);
			done(Ok(()));
		});
	}

	#[async_test]
	fn multiple_promises<F: FnOnce(Result<(), String>)>(done: F) {
		exec(async move {
			let results = futures::future::join_all(
				(0..10).map(|i| PromiseFuture::new(js! { return Promise.resolve(@{i}); })),
			)
			.await;
			for (i, res) in results.into_iter().enumerate() {
				let web_value = match res {
					Ok(v) => v,
					Err(e) => {
						dfail!(done, "Promise rejected with value: {:?}", e);
					}
				};
				dassert_eq!(done, web_value, i);
			}
			done(Ok(()));
		});
	}

	async_await_test!(
		fn http_req() {
			let req = stdweb::web::XmlHttpRequest::new();
			req.open("GET", "https://httpbin.org/get")?;

			let fut = PromiseFuture::new(js! {
				return new Promise(function(resolve, reject) {
					let req = @{req.as_ref().clone()};
					req.addEventListener("load", resolve);
					req.addEventListener("abort", function() { reject("abort"); });
					req.addEventListener("error", function() { reject("error"); });
					req.addEventListener("timeout", function() { reject("timeout"); });
				});
			});
			req.send()?;

			let _ev = fut.await.map_err(|e| e.into_string().unwrap())?;
			let body = req.response_text()?.unwrap();
			let value = serde_json::from_str::<serde_json::Value>(&body)?;

			let url_v = value
				.pointer("/url")
				.ok_or("Expected url field in response")?;
			let url = url_v
				.as_str()
				.ok_or("url field in response was not a string")?;
			if url != "https://httpbin.org/get" {
				return Err(
					format!("url field in response was incorrect, had value {:?}", url).into(),
				);
			}
			Ok(())
		}
	);
}
