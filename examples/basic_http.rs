use js_promises::*;

use std::panic;
use stdweb::{
	self,
	console,
	js,
};

fn panic_hook(info: &panic::PanicInfo) {
	let mut msg = info.to_string();
	let stack = js! {
		let old_limit = Error.stackTraceLimit;
		Error.stackTraceLimit = 99;
		let err = new Error();
		let stack = err.stack;
		Error.stackTraceLimit = old_limit;
		return stack;
	};
	msg.push_str("\n\nStack:\n\n");
	if let Some(s) = stack.as_str() {
		msg.push_str(s)
	} else {
		msg.push_str(&format!("{:?}", stack));
	}
	msg.push_str("\n\n");

	console!(error, msg);
}

pub fn set_panic_hook() {
	panic::set_hook(Box::new(panic_hook));
}

async fn http_req(url: &str) -> Result<serde_json::Value, String> {
	let req = stdweb::web::XmlHttpRequest::new();
	req.open("GET", url).expect("open failed");

	let fut = PromiseFuture::new(js! {
		return new Promise(function(resolve, reject) {
			let req = @{req.as_ref().clone()};
			req.addEventListener("load", resolve);
			req.addEventListener("abort", function() { reject("abort"); });
			req.addEventListener("error", function() { reject("error"); });
			req.addEventListener("timeout", function() { reject("timeout"); });
		});
	});
	req.send().unwrap();

	let res = fut.await;

	console!(log, "got web result");
	let _ev = res.map_err(|v| v.into_string().unwrap())?;

	let body = req.response_text().unwrap().unwrap();
	let out = serde_json::from_str::<serde_json::Value>(&body).unwrap();
	console!(log, "returning deserialized result");

	return Ok(out);
}

fn main() {
	set_panic_hook();
	exec(async move {
		let v = http_req("https://httpbin.org/get").await;
		console!(log, "request ok, value is:", format!("{:?}", v));
		let value = match v {
			Ok(v) => v,
			Err(e) => {
				console!(error, "Request failed", format!("{:?}", e));
				return;
			}
		};

		let ok = (|| {
			let url_v = value
				.pointer("/url")
				.ok_or(String::from("Expected url field in response"))?;
			let url = url_v
				.as_str()
				.ok_or(String::from("url field in response was not a string"))?;
			if url == "https://httpbin.org/get" {
				return Ok(());
			} else {
				return Err(format!(
					"url field in response was incorrect, had value {:?}",
					url
				));
			}
		})();
		if let Err(err) = ok {
			console!(error, "validation failed", err);
		}
	});
}
