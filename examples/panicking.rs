use js_promises::*;

use std::panic;
use stdweb::js;

fn main() {
	exec(async move {
		PromiseFuture::new(js! { return Promise.resolve(42); }).await;
		panic!("oof");
	});
}
